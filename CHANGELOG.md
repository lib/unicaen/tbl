CHANGELOG
=========

6.2.5 (09/01/2025)
------------------

- [Fix] Possibilité d'utiliser UnicaenApp >6


6.2.4 (03/12/2024)
------------------

- Possibilité d'ajouter des callbacks pour suivre en temps réel l'évolution des calculs

6.2.3 (20/08/2024)
------------------

- run doit retourner void
- cache d'entités dispo pour plus de perfs

6.2.2 (19/07/2024)
------------------

- Prise en compte des opérateurs <>, >=, <=, >, < et = pour les injections de paramètres
- Gestion du NOT NULL et du NOT IN

6.2.1 (13/06/2023)
------------------

- Rollback en cas d'exception lors d'une transaction doctrine

6.2.0 (11/12/2023)
------------------

- Compatibilité PHP8.2


6.1.0 (22/08/2023)
------------------

- Refonte du dispositif, plus aucun package utilisé

6.0.2 (19/06/2023)
------------------
- Pb de caractère "NULL" enlevé grâce à un TRIM

6.0.1 (23/03/2023)
------------------
- Dep Unicaen/Privilege


6.0.0 (03/02/2023)
------------------
- Dep Unicaen 6.0.*

5.0.3 (10/12/2021)
------------------
- Dep UnicaenApp 4.0.*

5.0.2 (30/11/2021)
-------------------
- Utilisation d'unicaen 4

5.0.1 (24/11/2021)
-------------------
- Nettoyage et mise aux normes PSR4


5.0 (23/11/2021)
-------------------
- Migration vers Laminas


4.2.1 (08/11/2021)
-------------------
- Oubli d'un privilège à retirer

4.2 (08/11/2021)
----------------
- Optimisation : on peut maintenant passer jusqu'à 5 critères pour actualiser un TBL


4.1 (03/11/2020)
----------------
- Correction de bug : il ne faut pas prendre en compte les colonnes cachées

4.0 (03/07/2020)
----------------
- Simplification du dispositif : on ne peut filtrer que sur un seul paramètre à la fois
- Optimisations importantes
- Intégration à la Zend Toolbar & à la console
