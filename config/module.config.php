<?php

namespace UnicaenTbl;

use UnicaenTbl\Options\ModuleOptions;
use UnicaenTbl\Process\DbDiffProcess;
use UnicaenTbl\Process\DbDiffProcessFactory;
use UnicaenTbl\Process\PlsqlProcess;
use UnicaenTbl\Process\PlsqlProcessFactory;
use UnicaenTbl\Service\BddService;
use UnicaenTbl\Service\BddServiceFactory;
use UnicaenTbl\Service\TableauBordService;


return [

    'unicaen-tbl' => [
        'tableaux_bord' => [],

        'entity_manager_name' => 'doctrine.entitymanager.orm_default',
    ],

    'service_manager' => [
        'aliases'   => [
            'unicaen-tbl.process.DbDiff' => DbDiffProcess::class,
            'unicaen-tbl.process.Plsql'  => PlsqlProcess::class,
        ],
        'factories' => [
            ModuleOptions::class         => Options\Factory\ModuleOptionsFactory::class,
            TableauBordService::class    => Service\TableauBordServiceFactory::class,
            BddService::class            => BddServiceFactory::class,

            DbDiffProcess::class => DbDiffProcessFactory::class,
            PlsqlProcess::class  => PlsqlProcessFactory::class,
        ],
    ],
];