<?php

namespace UnicaenTbl\Options\Factory;

use Psr\Container\ContainerInterface;
use UnicaenTbl\Options\ModuleOptions;

/**
 * Description of ModuleOptionsFactory
 *
 * @author Laurent LÉCLUSE <laurent.lecluse at unicaen.fr>
 */
class ModuleOptionsFactory
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $config = $container->get('Configuration');

        return new ModuleOptions(isset($config['unicaen-tbl']) ? $config['unicaen-tbl'] : []);
    }

}