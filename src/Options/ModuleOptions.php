<?php

namespace UnicaenTbl\Options;

use Laminas\Stdlib\AbstractOptions;
use Psr\Container\ContainerInterface;
use UnicaenTbl\TableauBord;


/**
 * Description of ModuleOptions
 *
 * @author LECLUSE Laurent <laurent.lecluse at unicaen.fr>
 */
class ModuleOptions extends AbstractOptions
{

    /**
     * Turn off strict options mode
     */
    protected $__strictMode__ = false;

    protected array $tableauxBord;

    protected string $entityManagerName;

    protected string $package;


    /**
     * @return string
     */
    public function getPackage()
    {
        return $this->package;
    }



    /**
     * @param string $package
     *
     * @return ModuleOptions
     */
    public function setPackage($package)
    {
        $this->package = $package;

        return $this;
    }



    /**
     * @return string
     */
    public function getEntityManagerName()
    {
        return $this->entityManagerName;
    }



    /**
     * @param string $entityManagerName
     *
     * @return ModuleOptions
     */
    public function setEntityManagerName($entityManagerName)
    {
        $this->entityManagerName = $entityManagerName;

        return $this;
    }



    /**
     * @param  $container
     * @return array|TableauBord[]
     * @throws \Exception
     */
    public function loadTableauxBord($container): array
    {
        $options = $this->getTableauxBord() ?? [];

        $tableauxBord = [];
        foreach ($options as $tblName => $tblOptions) {
            $tableauBord = new TableauBord();
            $tableauBord->setName($tblName);
            $tableauBord->loadOptions($tblOptions, $container);
            $tableauxBord[$tableauBord->getName()] = $tableauBord;
        }

        return $tableauxBord;
    }



    public function getTableauxBord(): ?array
    {
        return $this->tableauxBord;
    }



    public function setTableauxBord(array $tableauxBord): self
    {
        $this->tableauxBord = $tableauxBord;

        return $this;
    }
}