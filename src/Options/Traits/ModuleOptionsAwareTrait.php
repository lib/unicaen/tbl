<?php

namespace UnicaenTbl\Options\Traits;

use UnicaenTbl\Options\ModuleOptions;
use Application\Module;
use RuntimeException;

/**
 * Description of ModuleOptionsAwareTrait
 *
 * @author UnicaenCode
 */
trait ModuleOptionsAwareTrait
{
    /**
     * @var ModuleOptions
     */
    private $optionsModule;





    /**
     * @param ModuleOptions $optionsModule
     * @return self
     */
    public function setOptionsModule( ModuleOptions $optionsModule )
    {
        $this->optionsModule = $optionsModule;
        return $this;
    }



    /**
     * @return ModuleOptions
     * @throws RuntimeException
     */
    public function getOptionsModule()
    {
        return $this->optionsModule;
    }
}