<?php

namespace UnicaenTbl;

use UnicaenTbl\TableauBord;

class Event
{
    const CALCUL = 'calcul';
    const GET = 'get';
    const PROCESS = 'process';
    const SET = 'set';
    const FINISH = 'finish';
    const PROGRESS = 'progress';

    public TableauBord $tableauBord;

    public string $action;

    public ?int $progress = null;

    public ?int $total = null;
}