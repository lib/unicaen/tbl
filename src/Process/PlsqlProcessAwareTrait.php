<?php

namespace UnicaenTbl\Process;


/**
 * Description of PlsqlProcessAwareTrait
 *
 * @author UnicaenCode
 */
trait PlsqlProcessAwareTrait
{
    protected ?PlsqlProcess $processPlsql = null;



    /**
     * @param PlsqlProcess $processPlsql
     *
     * @return self
     */
    public function setProcessPlsql( ?PlsqlProcess $processPlsql )
    {
        $this->processPlsql = $processPlsql;

        return $this;
    }



    public function getProcessPlsql(): ?PlsqlProcess
    {
        return $this->processPlsql;
    }
}