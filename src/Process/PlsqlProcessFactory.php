<?php

namespace UnicaenTbl\Process;

use Psr\Container\ContainerInterface;
use UnicaenTbl\Service\BddService;


/**
 * Description of PlsqlProcessFactory
 *
 * @author Laurent Lécluse <laurent.lecluse at unicaen.fr>
 */
class PlsqlProcessFactory
{

    /**
     * @param ContainerInterface $container
     * @param string             $requestedName
     * @param array|null         $options
     *
     * @return PlsqlProcess
     */
    public function __invoke(ContainerInterface $container, $requestedName, $options = null): PlsqlProcess
    {
        $service = new PlsqlProcess;

        $service->setServiceBdd($container->get(BddService::class));

        return $service;
    }
}