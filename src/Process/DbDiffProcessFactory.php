<?php

namespace UnicaenTbl\Process;

use Psr\Container\ContainerInterface;
use UnicaenTbl\Service\BddService;


/**
 * Description of DbDiffProcessFactory
 *
 * @author LECLUSE Laurent <laurent.lecluse at unicaen.fr>
 */
class DbDiffProcessFactory
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $service = new DbDiffProcess();

        $service->setServiceBdd($container->get(BddService::class));

        return $service;
    }
}