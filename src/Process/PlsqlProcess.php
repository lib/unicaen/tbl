<?php

namespace UnicaenTbl\Process;

use UnicaenTbl\Service\BddServiceAwareTrait;
use UnicaenTbl\TableauBord;

/**
 * Description of PlsqlProcess
 *
 * @author LECLUSE Laurent <laurent.lecluse at unicaen.fr>
 */
class PlsqlProcess implements ProcessInterface
{
    use BddServiceAwareTrait;

    protected TableauBord $tableauBord;

    /**
     * Paramètres de filtres en entrée
     */
    protected array $params;

    /**
     * Commande à exécuter
     */
    protected string $command;




    public function run(TableauBord $tableauBord, array $params = []): void
    {
        $this->tableauBord = $tableauBord;
        $this->params = $params;

        $this->command = $tableauBord->getOption('command', null, 'Une commande PL/SQL doit être spécifiée');

        $conn = $this->getServiceBdd()->getEntityManager()->getConnection();
        $conn->beginTransaction();

        if (!empty($params)) {

            foreach ($params as $n => $v) {

                $plsql = "BEGIN $this->command(:n, :v); END;";
                $conn->executeStatement($plsql, compact('n', 'v'));

                // Seul le premier paramètre est pris en compte. Pour les autres c'est impossible
                break;
            }

        }else{
            $plsql = "BEGIN $this->command(); END;";
            $conn->executeStatement($plsql);
        }

        $conn->commit();
    }

}