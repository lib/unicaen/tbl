<?php

namespace UnicaenTbl\Process;

use UnicaenTbl\TableauBord;

/**
 * Description of ProcessInterface
 *
 * @author LECLUSE Laurent <laurent.lecluse at unicaen.fr>
 */
interface ProcessInterface
{
    public function run(TableauBord $tableauBord, array $params = []): void;
}