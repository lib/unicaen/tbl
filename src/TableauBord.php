<?php

namespace UnicaenTbl;

use Psr\Container\ContainerInterface;
use UnicaenTbl\Event;
use UnicaenTbl\Process\ProcessInterface;


/**
 * Description of TableauBord
 *
 * @author LECLUSE Laurent <laurent.lecluse at unicaen.fr>
 */
class TableauBord
{
    protected ?string $name = null;

    protected int $order = 0;

    protected ?ProcessInterface $process = null;

    private float $timingBegin = 0.0;

    protected float $timing = 0.0;

    /**
     * @var callable|null
     */
    private mixed $onAction = null;


    /** @var string[] */
    protected array $options = [];



    public function getName(): ?string
    {
        return $this->name;
    }



    public function setName(?string $name): TableauBord
    {
        $this->name = $name;

        return $this;
    }



    public function getOnAction(): ?callable
    {
        return $this->onAction;
    }



    public function setOnAction(?callable $onAction): TableauBord
    {
        $this->onAction = $onAction;
        return $this;
    }



    public function getTiming(): float
    {
        return $this->timing;
    }


    public function onAction(string $action, ?int $progress = null, ?int $total = null)
    {
        if (null !== $this->onAction) {
            $event              = new Event();
            $event->tableauBord = $this;
            $event->action      = $action;
            $event->progress    = $progress;
            $event->total       = $total;

            call_user_func($this->onAction, $event);
        }
    }



    public function getOrder(): int
    {
        return $this->order;
    }



    public function setOrder(int $order): TableauBord
    {
        $this->order = $order;

        return $this;
    }



    public function getProcess(): ?ProcessInterface
    {
        return $this->process;
    }



    public function setProcess(?ProcessInterface $process): TableauBord
    {
        $this->process = $process;

        return $this;
    }



    public function hasOption(string $name)
    {
        return array_key_exists($name, $this->options);
    }



    public function getOption(string $name, $defaultValue = null, ?string $errorIfNull = null)
    {
        $option = $this->options[$name] ?? $defaultValue;

        if (empty($option) && $errorIfNull) {
            throw new \Exception('Option ' . $name . ' manquante. ' . $errorIfNull);
        }

        return $option;
    }



    public function setOption(string $name, $value)
    {
        $this->options[$name] = $value;
    }



    public function loadOptions(array $options, ContainerInterface $container): self
    {
        if (array_key_exists('name', $options)) {
            $this->setName($options['name']);
        }

        if (array_key_exists('order', $options)) {
            $this->setOrder($options['order']);
        }

        if (array_key_exists('process', $options)) {
            $processName = $options['process'] ?? null;
            if (!$container->has($processName)) {
                $processName = 'unicaen-tbl.process.' . $processName;
            }
            if (!$container->has($processName)) {
                throw new \Exception('Le process "' . $processName . '" n\'est pas un service reconnu ou accessible via le container');
            }
            /** @var ProcessInterface $process */
            $process = $container->get($processName);
            $this->setProcess($process);
        }

        $this->options = $options;

        return $this;
    }



    public function calculer(array $params = [])
    {
        $this->timing = 0.0;
        $this->timingBegin = microtime(true);
        $this->onAction(Event::CALCUL);
        $this->getProcess()->run($this, $params);
        $this->timing = microtime(true) - $this->timingBegin;
        $this->onAction(Event::FINISH);
    }

}