<?php

namespace UnicaenTbl\Service;

use RuntimeException;

/**
 * Description of TableauBordServiceAwareTrait
 *
 * @author UnicaenCode
 */
trait TableauBordServiceAwareTrait
{
    /**
     * @var TableauBordService
     */
    private $serviceTableauBord;



    /**
     * @param TableauBordService $serviceTableauBord
     *
     * @return self
     */
    public function setServiceTableauBord(TableauBordService $serviceTableauBord)
    {
        $this->serviceTableauBord = $serviceTableauBord;

        return $this;
    }



    /**
     * @return TableauBordService
     * @throws RuntimeException
     */
    public function getServiceTableauBord()
    {
        return $this->serviceTableauBord;
    }
}

