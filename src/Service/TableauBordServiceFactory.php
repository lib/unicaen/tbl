<?php

namespace UnicaenTbl\Service;

use Psr\Container\ContainerInterface;
use UnicaenTbl\Options\ModuleOptions;


/**
 * Description of TableauBordServiceFactory
 *
 * @author LECLUSE Laurent <laurent.lecluse at unicaen.fr>
 */
class TableauBordServiceFactory
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $tbl = new TableauBordService();

        /** @var ModuleOptions $options */
        $options = $container->get(ModuleOptions::class);

        $tbl->setTableauxBord($options->loadTableauxBord($container));

        return $tbl;
    }
}