<?php

namespace UnicaenTbl\Service;

use UnicaenApp\Service\EntityManagerAwareTrait;

/**
 * Description of BddService
 *
 * @author Laurent Lécluse <laurent.lecluse at unicaen.fr>
 */
class BddService
{
    use EntityManagerAwareTrait;

    protected array $viewCache = [];

    protected array $entityCache = [];


    /**
     * Echappe une chaîne de caractères pour convertir en SQL
     *
     * @param string $string
     *
     * @return string
     */
    public function escapeKW($string)
    {
        return '"' . str_replace('"', '""', strtoupper($string)) . '"';
    }



    /**
     * Echappe une valeur pour convertir en SQL
     *
     * @param mixed $value
     *
     * @return string
     */
    public function escape($value): string
    {
        if (null === $value) return 'NULL';
        switch (gettype($value)) {
            case 'string':
                return "'" . str_replace("'", "''", $value) . "'";
            case 'integer':
                return (string)$value;
            case 'boolean':
                return $value ? '1' : '0';
            case 'double':
                return (string)$value;
            case 'array':
                return '(' . implode(',', array_map(__class__ . '::escape', $value)) . ')';
        }
        throw new \Exception('La valeur transmise ne peut pas être convertie en SQL');
    }



    /**
     * Retourne le code SQL correspondant à la valeur transmise, précédé de "=", "IS" ou "IN" suivant le contexte.
     *
     * @param mixed $value
     *
     * @return string
     */
    public function equals($value): string
    {
        if (null === $value) {
            $eq = ' IS ';
        } elseif (is_array($value)) $eq = ' IN ';
        else                        $eq = ' = ';

        return $eq . $this->escape($value);
    }



    /**
     * Retourne le code SQL "select" de la VUE
     *
     * @param string $viewName
     * @return string
     */
    public function getViewDefinition(string $viewName): string
    {
        if (!array_key_exists($viewName, $this->viewCache)) {
            $sql = "SELECT TEXT FROM USER_VIEWS WHERE VIEW_NAME = '" . $viewName . "'";
            $result = $this->query($sql, [], 'TEXT');

            $definition = $result[0];

            $definition = trim($definition);

            $this->viewCache[$viewName] = $definition;
        }

        return $this->viewCache[$viewName];
    }



    private function injectKeyMakeOpvar(string $opVar, $value): string
    {
        $operators = ['<>', '<=', '>=', '>', '<', '='];

        foreach( $operators as $operator ){
            if (str_starts_with($opVar, $operator)){
                break;
            }
        }

        $result = 'AND '.trim(substr( $opVar, strlen($operator)));


        if (null == $value){
            if ('<>' == $operator){
                return $result.' IS NOT NULL';
            }else{
                return $result.' IS NULL';
            }
        }

        if (is_array($value)){
            if ('=' == $operator){
                return $result.' IN '.$this->escape($value);
            }
            if ('<>' == $operator){
                return $result.' NOT IN '.$this->escape($value);
            }
            throw new \Exception('On ne peut pas confronter un tableau de données à l\'opérateur "'.$operator.'"');
        }

        $result .= ' '.$operator.' '.$this->escape($value);

        return $result;
    }


    /**
     * Transforme en filtre les commentaires de variables d'optimisation placés dans une requête
     *
     * @param string $query
     * @param array $key
     * @return string
     */
    public function injectKey(string $query, array $key): string
    {
        foreach( $key as $k => $v ){
            while(false !== ($start=strpos($query, '/*@'.$k))){
                $end = strpos($query, '*/', $start) + 2;

                $opVarStart = $start + strlen($k) + 3;

                $opVar = substr($query, $opVarStart, $end - $opVarStart - 2);

                $nf = $this->injectKeyMakeOpvar($opVar, $v);

                $query = substr_replace($query, $nf, $start, $end - $start);
            }
        }

        return $query;
    }



    public function makeWhere(array $key = []): string
    {
        $where = '';
        foreach( $key as $k => $v ){
            if ($where != '') $where .= ' AND ';
            $where .= $k.self::equals($v);
        }

        if ($where == ''){
            return '';
        }

        return 'WHERE '.$where;
    }



    /**
     * Retourne un tableau des résultats de la requête transmise.
     *
     *
     * @param string $sql
     * @param array $params
     * @param string $colRes
     *
     * @return array
     */
    protected function query($sql, $params = null, $colRes = null)
    {
        $stmt = $this->getEntityManager()->getConnection()->executeQuery($sql, $params);
        $result = [];
        while ($r = $stmt->fetchAssociative()) {
            if (empty($colRes)) $result[] = $r; else $result[] = $r[$colRes];
        }

        return $result;
    }



    public function sequenceNextVal(string $sequence): int
    {
        $dbConnection = $this->getEntityManager()->getConnection();
        $nextvalQuery = $dbConnection->getDatabasePlatform()->getSequenceNextValSQL($sequence);

        $stmt = $dbConnection->executeQuery($nextvalQuery);
        $res = (int)$stmt->fetchFirstColumn()[0];

        return $res;
    }



    public function entityGet(string $class, int $id): mixed
    {
        if (!isset($this->entityCache[$class][$id])){
            $this->entityCache[$class][$id] = $this->getEntityManager()->find($class, $id);
        }

        return $this->entityCache[$class][$id];
    }



    /**
     * exécute un ordre SQL
     *
     * @param string $sql
     *
     * @return integer
     */
    protected function exec($sql)
    {
        return $this->getEntityManager()->getConnection()->exec($sql);
    }
}