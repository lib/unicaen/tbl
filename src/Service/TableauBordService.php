<?php

namespace UnicaenTbl\Service;

use phpDocumentor\Reflection\Types\Mixed_;
use UnicaenTbl\TableauBord;

/**
 * Description of TableauBordService
 *
 * @author LECLUSE Laurent <laurent.lecluse at unicaen.fr>
 */
class TableauBordService
{
    /** @var array|TableauBord[] */
    protected array $tableauxBord = [];

    /**
     * @var callable|null
     */
    private mixed $onAction = null;



    /**
     * @return array|TableauBord[]
     */
    public function getTableauxBord(): array
    {
        return $this->tableauxBord;
    }



    public function hasTableauBord(string $name): bool
    {
        return array_key_exists($name, $this->tableauxBord);
    }



    public function getTableauBord(string $name): TableauBord
    {
        $this->tableauxBord[$name]->setOnAction($this->getOnAction());
        return $this->tableauxBord[$name];
    }



    public function setTableauxBord(array $tableauxBord): TableauBordService
    {
        uasort($tableauxBord, function ($a, $b) {
            return $a->getOrder() - $b->getOrder();
        });
        $this->tableauxBord = $tableauxBord;
        return $this;
    }



    public function getOnAction(): ?callable
    {
        return $this->onAction;
    }



    public function setOnAction(?callable $onAction): TableauBordService
    {
        $this->onAction = $onAction;
        return $this;
    }



    public function calculer(string|TableauBord $tableauBord, array $params = []): self
    {
        if (is_string($tableauBord)) {
            $tableauBord = $this->getTableauBord($tableauBord);
        }

        $tableauBord->calculer($params);

        return $this;
    }



    public function calculerTout(array $excludes = []): bool
    {
        $tableauxBord = $this->getTableauxBord();

        $result = true;
        foreach ($tableauxBord as $tableauBord) {
            if (in_array($tableauBord->getName(), $excludes)) {
                continue;
            }
            try {
                $tableauBord->setOnAction($this->getOnAction());
                $tableauBord->calculer([]);
            } catch (\Exception $e) {
                $result = false;
            }
        }

        return $result;
    }
}