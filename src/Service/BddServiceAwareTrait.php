<?php

namespace UnicaenTbl\Service;


/**
 * Description of BddServiceAwareTrait
 *
 * @author UnicaenCode
 */
trait BddServiceAwareTrait
{
    protected ?BddService $serviceBdd = null;



    /**
     * @param BddService $serviceBdd
     *
     * @return self
     */
    public function setServiceBdd(?BddService $serviceBdd)
    {
        $this->serviceBdd = $serviceBdd;

        return $this;
    }



    public function getServiceBdd(): ?BddService
    {
        return $this->serviceBdd;
    }
}