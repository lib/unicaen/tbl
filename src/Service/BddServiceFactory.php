<?php

namespace UnicaenTbl\Service;

use Psr\Container\ContainerInterface;
use UnicaenTbl\Options\ModuleOptions;


/**
 * Description of BddServiceFactory
 *
 * @author Laurent Lécluse <laurent.lecluse at unicaen.fr>
 */
class BddServiceFactory
{

    /**
     * @param ContainerInterface $container
     * @param string             $requestedName
     * @param array|null         $options
     *
     * @return BddService
     */
    public function __invoke(ContainerInterface $container, $requestedName, $options = null): BddService
    {
        $service = new BddService;

        /** @var ModuleOptions $options */
        $options = $container->get(ModuleOptions::class);

        $em = $container->get($options->getEntityManagerName());
        /* @var $em \Doctrine\ORM\EntityManager */

        $service->setEntityManager($em);

        return $service;
    }
}