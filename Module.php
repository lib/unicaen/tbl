<?php

namespace UnicaenTbl;

use Laminas\ModuleManager\Feature\ConfigProviderInterface;

/**
 *
 *
 * @author Laurent LECLUSE <laurent.lecluse at unicaen.fr>
 */
class Module implements ConfigProviderInterface
{
    public function getConfig()
    {
        return include __DIR__ . '/config/module.config.php';
    }
}
